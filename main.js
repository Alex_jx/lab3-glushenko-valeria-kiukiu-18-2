
function toBottom(elem)
{
    var parent = $(elem).parent();
    var parent_html = $(parent).html();
    $(parent).remove();
    $('.main-container').append('<div class="block">' + parent_html + '</div>');
}

function toTop(elem)
{
    var parent = $(elem).parent();
    var parent_html = $(parent).html();
    $(parent).remove();
    $('.main-container').prepend('<div class="block">'+parent_html+'</div>');
}

function scaleMinus(elem)
{
    var parent = $(elem).parent();
    var img = $(parent).children().first();
    var def_height = $(img).height();
    var def_width = $(img).width();
    if(def_width>20){
        def_width -= 10;
        def_height -= 10;
        $(img).css('width', def_width);
        $(img).css('height', def_height);
    }
}

function scalePlus(elem)
{
    var parent = $(elem).parent();
    var img = $(parent).children().first();
    var def_height = $(img).height();
    var def_width = $(img).width();
    if(def_width<370) {
        def_width += 10;
        def_height += 10;
        $(img).css('width', def_width);
        $(img).css('height', def_height);
    }
}
